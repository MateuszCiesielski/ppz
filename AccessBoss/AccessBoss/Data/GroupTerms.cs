﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class GroupTerms
    {
        public GroupTerms()
        {
            Attendances = new HashSet<Attendances>();
        }

        public int GroupTermId { get; set; }
        public int GroupId { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }

        public virtual Groups Group { get; set; }
        public virtual ICollection<Attendances> Attendances { get; set; }
    }
}
