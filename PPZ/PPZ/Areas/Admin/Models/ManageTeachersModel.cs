﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Admin.Models
{
    public class ManageTeachersModel
    {
        public Group Group { get; set; }
        public List<Teacher> AssignedTeachers { get; set; }
        public List<Teacher> UnassignedTeachers { get; set; }
        public bool Assign { get; set; }
        public Teacher Teacher { get; set; }
    }
}
