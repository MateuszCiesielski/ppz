﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class Post
    {
        public int PostId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }

        [Spam]
        public string Message { get; set; }
        public DateTime DateTime { get; set; }
    }
}
