﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PPZ.Data
{
    public class Guardian
    {
        public int GuardianId { get; set; }
        [Required]
        [Display(Name = "First name*")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last name*")]
        public string LastName { get; set; }
        public string FullName => FirstName + " " + LastName;
        public string Address { get; set; }
        [Display(Name = "Email address")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        [Display(Name = "Phone number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public List<StudentGuardian> StudentGuardians { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
