﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Models
{
    public class MessagesModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public DateTime Time { get; set; }
        public string ShortContent { get; set; }
    }
}
