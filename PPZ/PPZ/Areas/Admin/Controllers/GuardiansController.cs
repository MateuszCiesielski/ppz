﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Admin.Models;
using PPZ.Data;

namespace PPZ.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class GuardiansController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public GuardiansController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Admin/Guardians
        public async Task<IActionResult> Index()
        {
            return View(await _context.Guardians.ToListAsync());
        }

        public IActionResult ImportCsv()
        {
            CsvModel csvModel = new CsvModel();
            return View(csvModel);
        }

        [HttpPost]
        public async Task<IActionResult> ImportCsv(CsvModel csvModel)
        {

            string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "csv");
            string fileName = csvModel.FormFile.FileName;
            string filePath = Path.Combine(uploadsFolder, fileName);

            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await csvModel.FormFile.CopyToAsync(fileStream);
            }

            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.RegisterClassMap<GuardianCsvMap>();
                var guardians = csv.GetRecords<Guardian>();

                try
                {
                    await _context.AddRangeAsync(guardians);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    var model = new CsvErrorPageModel() { ErrorMessage = e.Message, FilePath = filePath };
                    return View("../CsvErrorPage", model);
                }
            }

            System.IO.File.Delete(filePath);

            return RedirectToAction(nameof(Index));
        }

        // GET: Admin/Guardians/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guardian = await _context.Guardians
                .FirstOrDefaultAsync(m => m.GuardianId == id);
            if (guardian == null)
            {
                return NotFound();
            }

            return View(guardian);
        }

        // GET: Admin/Guardians/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Guardians/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GuardianId,FirstName,LastName,Address,EmailAddress,PhoneNumber")] Guardian guardian)
        {
            if (ModelState.IsValid)
            {
                _context.Add(guardian);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(guardian);
        }

        // GET: Admin/Guardians/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guardian = await _context.Guardians.FindAsync(id);
            if (guardian == null)
            {
                return NotFound();
            }
            return View(guardian);
        }

        // POST: Admin/Guardians/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GuardianId,FirstName,LastName,Address,EmailAddress,PhoneNumber")] Guardian guardian)
        {
            if (id != guardian.GuardianId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(guardian);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GuardianExists(guardian.GuardianId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(guardian);
        }

        // GET: Admin/Guardians/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guardian = await _context.Guardians
                .FirstOrDefaultAsync(m => m.GuardianId == id);
            if (guardian == null)
            {
                return NotFound();
            }

            return View(guardian);
        }

        // POST: Admin/Guardians/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var guardian = await _context.Guardians.FindAsync(id);
            var user = await _context.ApplicationUsers.Where(u => u.GuardianId == id).SingleOrDefaultAsync();
            if (user != null)
            {
                await _userManager.RemoveFromRoleAsync(user, "Guardian");
                user.GuardianId = null;
                _context.Update(user);
            }
            var studentGuardians = await _context.StudentGuardians.Where(sg => sg.GuardianId == id).ToListAsync();
            _context.StudentGuardians.RemoveRange(studentGuardians);
            _context.Guardians.Remove(guardian);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GuardianExists(int id)
        {
            return _context.Guardians.Any(e => e.GuardianId == id);
        }
    }
}
