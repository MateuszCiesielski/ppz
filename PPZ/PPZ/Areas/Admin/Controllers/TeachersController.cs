﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Admin.Models;
using PPZ.Data;

namespace PPZ.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]    
    public class TeachersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public TeachersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Admin/Teachers
        public async Task<IActionResult> Index()
        {
            return View(await _context.Teachers.Where(t => t.Approved == true).ToListAsync());            
        }

        public IActionResult ImportCsv()
        {
            CsvModel csvModel = new CsvModel();
            return View(csvModel);
        }

        [HttpPost]
        public async Task<IActionResult> ImportCsv(CsvModel csvModel)
        {

            string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "csv");
            string fileName = csvModel.FormFile.FileName;
            string filePath = Path.Combine(uploadsFolder, fileName);

            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await csvModel.FormFile.CopyToAsync(fileStream);
            }

            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.RegisterClassMap<TeacherCsvMap>();
                var teachers = csv.GetRecords<Teacher>();

                try
                {
                    await _context.AddRangeAsync(teachers);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    var model = new CsvErrorPageModel() { ErrorMessage = e.Message, FilePath = filePath };
                    return View("../CsvErrorPage", model);
                }
            }

            System.IO.File.Delete(filePath);

            return RedirectToAction(nameof(Index));
        }


        // GET: Admin/Teachers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teachers
                .FirstOrDefaultAsync(m => m.TeacherId == id);

            if (teacher == null)
            {
                return NotFound();
            }

            var groups = await _context.Groups
                .Join(_context.GroupTeachers, g => g.GroupId, gs => gs.GroupId, (g, gs) => new { g, gs })
                .Where(ggs => ggs.gs.TeacherId == id)
                .Select(ggs => ggs.g)
                .ToListAsync();

            return View(new TeacherModel
            {
                Teacher = teacher,
                Groups = groups
            });
        }

        // GET: Admin/Teachers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Teachers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TeacherId,Prefix,FirstName,LastName,EmailAddress,PhoneNumber,Info")] Teacher teacher)
        {
            if (ModelState.IsValid)
            {
                teacher.Approved = true;
                _context.Add(teacher);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(teacher);
        }

        // GET: Admin/Teachers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teachers.FindAsync(id);
            if (teacher == null)
            {
                return NotFound();
            }
            return View(teacher);
        }

        // POST: Admin/Teachers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TeacherId,Prefix,FirstName,LastName,EmailAddress,PhoneNumber,Info")] Teacher teacher)
        {
            if (id != teacher.TeacherId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(teacher);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeacherExists(teacher.TeacherId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(teacher);
        }

        // GET: Admin/Teachers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teachers
                .FirstOrDefaultAsync(m => m.TeacherId == id);
            if (teacher == null)
            {
                return NotFound();
            }

            return View(teacher);
        }

        // POST: Admin/Teachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var teacher = await _context.Teachers.FindAsync(id);
            var user = await _context.ApplicationUsers.Where(u => u.TeacherId == id).SingleOrDefaultAsync();
            if (user != null)
            {
                await _userManager.RemoveFromRoleAsync(user, "Teacher");
                user.TeacherId = null;                
                _context.Update(user);
            }
            var groupTeachers = await _context.GroupTeachers.Where(gt => gt.TeacherId == id).ToListAsync();
            _context.GroupTeachers.RemoveRange(groupTeachers);
            _context.Teachers.Remove(teacher);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Admin/Teachers/Approval/5
        public async Task<IActionResult> Approval(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teachers
                .FirstOrDefaultAsync(m => m.TeacherId == id);
            if (teacher == null)
            {
                return NotFound();
            }

            return View(teacher);
        }

        // POST: Admin/Teachers/Approval/5
        [HttpPost, ActionName("Approval")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApprovalConfirmed(int id)
        {
            var teacher = await _context.Teachers.FindAsync(id);
            var user = await _context.ApplicationUsers.Where(u => u.TeacherId == id).SingleOrDefaultAsync();
            if (user != null)
            {
                await _userManager.AddToRoleAsync(user, "Teacher");
            }
            teacher.Approved = true;
            _context.Update(teacher);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TeacherExists(int id)
        {
            return _context.Teachers.Any(e => e.TeacherId == id);
        }
    }
}
