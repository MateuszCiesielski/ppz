﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Admin.Models
{
    public class RFIDMapModel
    {
        public RFIDMap rfidMap { get; set; }
        public List<Student> StudentList { get; set; }
        public Student Student { get; set; }
    }
}
