﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Rfidmap
    {
        public int RfidmapId { get; set; }
        public string KeyId { get; set; }
        public int StudentId { get; set; }

        public virtual Students Student { get; set; }
    }
}
