﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PPZ.Migrations
{
    public partial class Themes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(

                table: "Themes",

                columns: new[] { "ThemeId", "Name", "Path" },

                values: new object[,]

                {

                    { 1, "Cerulean", "~/lib/bootstrap/dist/css/cerulean-bootstrap.css" },

                    { 19, "Superhero", "~/lib/bootstrap/dist/css/superhero-bootstrap.css" },

                    { 18, "Spacelab", "~/lib/bootstrap/dist/css/spacelab-bootstrap.css" },

                    { 17, "Solar", "~/lib/bootstrap/dist/css/solar-bootstrap.css" },

                    { 16, "Slate", "~/lib/bootstrap/dist/css/slate-bootstrap.css" },

                    { 15, "Sketchy", "~/lib/bootstrap/dist/css/sketchy-bootstrap.css" },

                    { 14, "Simplex", "~/lib/bootstrap/dist/css/simplex-bootstrap.css" },

                    { 13, "Sandstone", "~/lib/bootstrap/dist/css/sandstone-bootstrap.css" },

                    { 12, "Pulse", "~/lib/bootstrap/dist/css/pulse-bootstrap.css" },

                    { 20, "United", "~/lib/bootstrap/dist/css/united-bootstrap.css" },

                    { 11, "Minty", "~/lib/bootstrap/dist/css/minty-bootstrap.css" },

                    { 9, "Lux", "~/lib/bootstrap/dist/css/lux-bootstrap.css" },

                    { 8, "Lumen", "~/lib/bootstrap/dist/css/lumen-bootstrap.css" },

                    { 7, "Litera", "~/lib/bootstrap/dist/css/litera-bootstrap.css" },

                    { 6, "Journal", "~/lib/bootstrap/dist/css/journal-bootstrap.css" },

                    { 5, "Flatly", "~/lib/bootstrap/dist/css/flatly-bootstrap.css" },

                    { 4, "Darkly", "~/lib/bootstrap/dist/css/darkly-bootstrap.css" },

                    { 3, "Cyborg", "~/lib/bootstrap/dist/css/cyborg-bootstrap.css" },

                    { 2, "Cosmo", "~/lib/bootstrap/dist/css/cosmo-bootstrap.css" },

                    { 10, "Materia", "~/lib/bootstrap/dist/css/materia-bootstrap.css" },

                    { 21, "Yeti", "~/lib/bootstrap/dist/css/yeti-bootstrap.css" }

                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(

                name: "Themes");
        }
    }
}
