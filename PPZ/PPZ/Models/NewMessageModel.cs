﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Models
{
    public class NewMessageModel
    {
        public AdminInfo Admin { get; set; }
        public List<GroupInfo> Contacts { get; set; }

        [Required]
        public string Message { get; set; }
    }

    public class GroupInfo
    {
        public Group Group { get; set; }
        public List<TeacherInfo> Teachers { get; set; }
        public List<StudentGuardianInfo> StudentsAndGuardians { get; set; }
    }

    public class AdminInfo
    {
        public string AccountId { get; set; }
        public bool Send { get; set; }
    }

    public class TeacherInfo
    {
        public Teacher Teacher { get; set; }
        public string AccountId { get; set; }
        public bool Send { get; set; }
    }

    public class StudentGuardianInfo
    {
        public Student Student { get; set; }
        public string StudentAccountId { get; set; }
        public bool StudentSend { get; set; }
        public Guardian Guardian { get; set; }
        public string GuardianAccountId { get; set; }
        public bool GuardianSend { get; set; }
    }
}
