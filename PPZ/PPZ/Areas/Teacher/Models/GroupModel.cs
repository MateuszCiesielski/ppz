﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class GroupModel
    {
        public Group Group { get; set; }
        public List<Teacher> GroupTeachers { get; set; }
        public List<Student> GroupStudents { get; set; }
    }
}
