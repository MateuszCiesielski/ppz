﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class AccessKey
    {
        public int AccessKeyId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Key { get; set; }
        public string KeyHash { get; set; }
        public int StudentId { get; set; }
        public bool Guardian { get; set; }

        public virtual Student Student { get; set; }
    }
}
