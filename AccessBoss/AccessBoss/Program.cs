﻿using Microsoft.EntityFrameworkCore;
using ModuleListener.Data;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AccessBoss
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string location = "S20";
            string inputFile = "/path/to/rfid.in";

            Led led = new Led();

            while (true)
            {
                bool passed = false;
                string entry = null;
                
                while (entry == null)
                {
                    try
                    {
                        entry = File.ReadAllText(inputFile);
                    }
                    catch { }
                }

                File.Delete(inputFile);

                string id;
                DateTime date;

                try
                {
                    var entrySplit = entry.Split(new char[] { ',', '.' });
                    id = entrySplit[0];
                    date = DateTime.Parse(entrySplit[1]);
                }
                catch
                {
                    continue;
                }

                using (var context = new myamDBContext())
                {
                    var student = await context.Rfidmap
                        .Join(context.Students, r => r.StudentId, s => s.StudentId, (r, s) => new { r, s })
                        .Where(rs => rs.r.KeyId == id)
                        .Select(rs => rs.s)
                        .SingleOrDefaultAsync();

                    if (student == null)
                    {
                        Console.WriteLine("NO_ID");
                        led.Denied();
                        continue;
                    }

                    var groups = await context.Groups
                        .Join(context.GroupStudents, g => g.GroupId, gs => gs.GroupId, (g, gs) => new { g, gs })
                        .Join(context.Students, ggs => ggs.gs.StudentId, s => s.StudentId, (ggs, s) => new { ggs, s })
                        .Where(ggss => ggss.s.StudentId == student.StudentId)
                        .Select(ggss => ggss.ggs.g)
                        .ToListAsync();

                    foreach (var group in groups)
                    {
                        if (group.Location == location)
                        {
                            var groupTerms = await context.GroupTerms
                                .Where(gt => gt.GroupId == group.GroupId)
                                .ToListAsync();

                            foreach (var groupTerm in groupTerms)
                            {
                                if (date < groupTerm.DateEnd && date > groupTerm.DateStart - TimeSpan.FromMinutes(15))
                                {
                                    bool isLate = false;
                                    if (date > groupTerm.DateStart)
                                    {
                                        isLate = true;
                                    }

                                    var attendance = await context.Attendances
                                        .Where(a => a.GroupTermId == groupTerm.GroupTermId)
                                        .Where(a => a.StudentId == student.StudentId)
                                        .SingleOrDefaultAsync();

                                    if (attendance == null)
                                    {
                                        attendance = new Attendances
                                        {
                                            GroupTermId = groupTerm.GroupTermId,
                                            IsLate = isLate,
                                            StudentId = student.StudentId
                                        };
                                        context.Attendances.Add(attendance);
                                        await context.SaveChangesAsync();
                                    }

                                    Console.WriteLine("GRANTED");
                                    led.Granted();
                                    passed = true;
                                    break;
                                }
                            }
                        }
                    }
                }

                if (passed == false)
                {
                    Console.WriteLine("DENIED");
                    led.Denied();
                }
            }
        }
    }
}
