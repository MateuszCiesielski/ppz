﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Admin.Models
{
    public class CsvModel
    {
        [Required(ErrorMessage = "Please select a file.")]
        public IFormFile FormFile { get; set; }
    }
}
