﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class AspNetUsers
    {
        public AspNetUsers()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaims>();
            AspNetUserLogins = new HashSet<AspNetUserLogins>();
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
            AspNetUserTokens = new HashSet<AspNetUserTokens>();
            MessagesReceiverUser = new HashSet<Messages>();
            MessagesSenderUser = new HashSet<Messages>();
            Posts = new HashSet<Posts>();
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEnd { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string Discriminator { get; set; }
        public string AvatarPath { get; set; }
        public int? GuardianId { get; set; }
        public int? StudentId { get; set; }
        public int? TeacherId { get; set; }
        public string ThemePath { get; set; }

        public virtual Guardians Guardian { get; set; }
        public virtual Students Student { get; set; }
        public virtual Teachers Teacher { get; set; }
        public virtual ICollection<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual ICollection<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual ICollection<Messages> MessagesReceiverUser { get; set; }
        public virtual ICollection<Messages> MessagesSenderUser { get; set; }
        public virtual ICollection<Posts> Posts { get; set; }
    }
}
