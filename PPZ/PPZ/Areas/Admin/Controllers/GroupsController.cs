﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Admin.Models;
using PPZ.Data;

namespace PPZ.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class GroupsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public GroupsController(ApplicationDbContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Admin/Groups
        public async Task<IActionResult> Index()
        {
            return View(await _context.Groups.ToListAsync());
        }

        public IActionResult ImportCsv()
        {
            CsvModel csvModel = new CsvModel();
            return View(csvModel);
        }

        [HttpPost]
        public async Task<IActionResult> ImportCsv(CsvModel csvModel)
        {

            string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "csv");
            string fileName = csvModel.FormFile.FileName;
            string filePath = Path.Combine(uploadsFolder, fileName);

            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await csvModel.FormFile.CopyToAsync(fileStream);
            }

            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.RegisterClassMap<GroupCsvMap>();
                var groups = csv.GetRecords<Group>();

                try
                {
                    await _context.AddRangeAsync(groups);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    var model = new CsvErrorPageModel() { ErrorMessage = e.Message, FilePath = filePath };
                    return View("../CsvErrorPage", model);
                }
            }

            System.IO.File.Delete(filePath);

            return RedirectToAction(nameof(Index));
        }

        // GET: Admin/Groups/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @group = await _context.Groups
                .FirstOrDefaultAsync(m => m.GroupId == id);
            if (@group == null)
            {
                return NotFound();
            }

            var teachers = await _context.Teachers.ToListAsync();

            var groupTeachers = teachers
                .Join(_context.GroupTeachers, t => t.TeacherId, gt => gt.TeacherId, (t, gt) => new { t, gt })
                .Where(tgt => tgt.gt.GroupId == id)
                .Select(tgt => tgt.t)
                .ToList();

            var students = await _context.Students.ToListAsync();

            var groupStudents = students
                .Join(_context.GroupStudents, s => s.StudentId, gs => gs.StudentId, (s, gs) => new { s, gs })
                .Where(sgs => sgs.gs.GroupId == id)
                .Select(sgs => sgs.s)
                .ToList();

            return View(new GroupModel 
            { 
                Group = @group,
                GroupTeachers = groupTeachers,
                GroupStudents = groupStudents
            });
        }

        // GET: Admin/Groups/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Groups/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GroupId,Name,Location,Info")] Group @group)
        {
            if (ModelState.IsValid)
            {
                _context.Add(@group);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(@group);
        }

        // GET: Admin/Groups/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @group = await _context.Groups.FindAsync(id);
            if (@group == null)
            {
                return NotFound();
            }
            return View(@group);
        }

        // POST: Admin/Groups/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GroupId,Name,Location,Info")] Group @group)
        {
            if (id != @group.GroupId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(@group);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GroupExists(@group.GroupId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(@group);
        }

        // GET: Admin/Groups/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @group = await _context.Groups
                .FirstOrDefaultAsync(m => m.GroupId == id);
            if (@group == null)
            {
                return NotFound();
            }

            return View(@group);
        }

        // POST: Admin/Groups/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var @group = await _context.Groups.FindAsync(id);
            var attendances = await _context.Attendances
                .Join(_context.GroupTerms, a => a.GroupTermId, gt => gt.GroupTermId, (a, gt) => new { a, gt })
                .Where(agt => agt.gt.GroupId == id)
                .Select(agt => agt.a)
                .ToListAsync();
            _context.Attendances.RemoveRange(attendances);
            var groupTerms = await _context.GroupTerms.Where(gt => gt.GroupId == id).ToListAsync();
            _context.GroupTerms.RemoveRange(groupTerms);
            var groupStudents = await _context.GroupStudents.Where(gt => gt.GroupId == id).ToListAsync();
            _context.GroupStudents.RemoveRange(groupStudents);
            var groupTeachers = await _context.GroupTeachers.Where(gt => gt.GroupId == id).ToListAsync();
            _context.GroupTeachers.RemoveRange(groupTeachers);
            _context.Groups.Remove(@group);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Admin/Groups/ManageTeachers/5
        public async Task<IActionResult> ManageTeachers(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @group = await _context.Groups.FindAsync(id);

            if (@group == null)
            {
                return NotFound();
            }

            var assignedTeachers = await _context.Teachers
                .Join(_context.GroupTeachers, t => t.TeacherId, gt => gt.TeacherId, (t, gt) => new { t, gt })
                .Where(tgt => tgt.gt.GroupId == id)
                .Select(tgt => tgt.t)
                .ToListAsync();

            var unassignedTeachers = (await _context.Teachers.ToListAsync())
                .Except(assignedTeachers)
                .ToList();

            return View(new ManageTeachersModel { 
                Group = @group,
                AssignedTeachers = assignedTeachers,
                UnassignedTeachers = unassignedTeachers
            });
        }

        // POST: Admin/Groups/ManageTeachers
        [HttpPost]
        public async Task<IActionResult> ManageTeachers(ManageTeachersModel model)
        {
            var @group = await _context.Groups.FindAsync(model.Group.GroupId);
            var teacher = await _context.Teachers.FindAsync(model.Teacher.TeacherId);

            if (@group == null || teacher == null)
            {
                return NotFound();
            }

            if (model.Assign)
            {
                var groupTeacher = new GroupTeacher
                {
                    GroupId = @group.GroupId,
                    TeacherId = teacher.TeacherId
                };
                _context.Add(groupTeacher);
            }
            else
            {
                var groupTeacher = await _context.GroupTeachers
                    .Where(gt => gt.TeacherId == teacher.TeacherId)
                    .Where(gt => gt.GroupId == @group.GroupId)
                    .ToListAsync();
                _context.RemoveRange(groupTeacher);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(ManageTeachers), @group.GroupId);
        }

        // GET: Admin/Groups/ManageStudents/5
        public async Task<IActionResult> ManageStudents(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @group = await _context.Groups.FindAsync(id);

            if (@group == null)
            {
                return NotFound();
            }

            var assignedStudents = await _context.Students
                .Join(_context.GroupStudents, t => t.StudentId, gt => gt.StudentId, (t, gt) => new { t, gt })
                .Where(tgt => tgt.gt.GroupId == id)
                .Select(tgt => tgt.t)
                .ToListAsync();

            var unassignedStudents = (await _context.Students.ToListAsync())
                .Except(assignedStudents)
                .ToList();

            return View(new ManageStudentsModel
            {
                Group = @group,
                AssignedStudents = assignedStudents,
                UnassignedStudents = unassignedStudents
            });
        }

        // POST: Admin/Groups/ManageStudents
        [HttpPost]
        public async Task<IActionResult> ManageStudents(ManageStudentsModel model)
        {
            var @group = await _context.Groups.FindAsync(model.Group.GroupId);
            var student = await _context.Students.FindAsync(model.Student.StudentId);

            if (@group == null || student == null)
            {
                return NotFound();
            }

            if (model.Assign)
            {
                var groupStudent = new GroupStudent
                {
                    GroupId = @group.GroupId,
                    StudentId = student.StudentId
                };
                _context.Add(groupStudent);
            }
            else
            {
                var groupStudent = await _context.GroupStudents
                    .Where(gt => gt.StudentId == student.StudentId)
                    .Where(gt => gt.GroupId == @group.GroupId)
                    .ToListAsync();
                _context.RemoveRange(groupStudent);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(ManageStudents), @group.GroupId);
        }

        // GET: Admin/Groups/ManageTerms/5
        public async Task<IActionResult> ManageTerms(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @group = await _context.Groups.FindAsync(id);

            if (@group == null)
            {
                return NotFound();
            }

            var terms = await _context.GroupTerms
                .Where(gt => gt.GroupId == id)
                .ToListAsync();

            return View(new ManageTermsModel
            {
                Group = @group,
                GroupTerms = terms
            });
        }

        // POST: Admin/Groups/ManageTerms
        [HttpPost]
        public async Task<IActionResult> ManageTerms(ManageTermsModel model)
        {
            var @group = await _context.Groups.FindAsync(model.Group.GroupId);

            if (@group == null)
            {
                return NotFound();
            }

            if (model.Action == 0)
            {
                if (model.SingleTimeEnd <= model.SingleTimeStart)
                {
                    ModelState.AddModelError("SingleDate", "The end time cannot be lower than the start time.");
                    return View(model);
                }

                DateTime dateStart = (DateTime)model.SingleDate;
                dateStart = dateStart.Add(model.SingleTimeStart);
                DateTime dateEnd = (DateTime)model.SingleDate;
                dateEnd = dateEnd.Add(model.SingleTimeEnd);

                var groupTerm = new GroupTerm
                {
                    GroupId = @group.GroupId,
                    DateStart = dateStart,
                    DateEnd = dateEnd
                };
                _context.Add(groupTerm);
            }
            else if (model.Action == 1)
            {
                if (model.RangeTimeEnd <= model.RangeTimeStart)
                {
                    ModelState.AddModelError("RangeDayOfWeek", "The end time cannot be lower than the start time.");
                    return View(model);
                }
                if (model.RangeDateEnd <= model.RangeDateStart)
                {
                    ModelState.AddModelError("RangeDateStart", "The end date cannot be lower than the start date.");
                    return View(model);
                }

                DateTime date = (DateTime)model.RangeDateStart;

                while (date <= model.RangeDateEnd)
                {
                    if (date.DayOfWeek == model.RangeDayOfWeek)
                    {
                        DateTime dateStart = date;
                        dateStart = dateStart.Add(model.RangeTimeStart);
                        DateTime dateEnd = date;
                        dateEnd = dateEnd.Add(model.RangeTimeEnd);

                        var groupTerm = new GroupTerm
                        {
                            GroupId = @group.GroupId,
                            DateStart = dateStart,
                            DateEnd = dateEnd
                        };
                        _context.Add(groupTerm);
                    }
                    date = date.AddDays(1);
                }
            }
            else if (model.Action == 2)
            {
                var groupTerm = await _context.GroupTerms.FindAsync(model.GroupTermId);
                _context.GroupTerms.Remove(groupTerm);
            }
            else
            {
                var terms = await _context.GroupTerms
                    .Where(gt => gt.GroupId == @group.GroupId)
                    .ToListAsync();

                _context.GroupTerms.RemoveRange(terms);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(ManageTerms), @group.GroupId);
        }

        // GET: Admin/Groups/Attendance/5
        public async Task<IActionResult> Attendance(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var groupTerm = await _context.GroupTerms.FindAsync(id);

            if (groupTerm == null)
            {
                return NotFound();
            }

            var groupName = await _context.Groups
                .Where(g => g.GroupId == groupTerm.GroupId)
                .Select(g => g.Name)
                .SingleOrDefaultAsync();

            if (groupName == null)
            {
                return NotFound();
            }

            var students = await _context.Students
                .Join(_context.GroupStudents, s => s.StudentId, gs => gs.StudentId, (s, gs) => new { s, gs })
                .Where(sgs => sgs.gs.GroupId == groupTerm.GroupId)
                .Select(sgs => sgs.s)
                .ToListAsync();

            List<Tuple<Student, int>> studentsAttendances = new List<Tuple<Student, int>>();

            foreach (var student in students)
            {
                int status = 0;
                var attendance = await _context.Attendances
                    .Where(a => a.GroupTermId == id)
                    .Where(a => a.StudentId == student.StudentId)
                    .SingleOrDefaultAsync();
                if (attendance != null)
                {
                    if (attendance.IsLate)
                        status = 1;
                    else
                        status = 2;
                }
                studentsAttendances.Add(Tuple.Create(student, status));
            }

            return View(new AttendanceModel
            {
                GroupTerm = groupTerm,
                GroupName = groupName,
                StudentsAttendances = studentsAttendances
            });
        }

        // POST: Admin/Groups/Attendance
        [HttpPost]
        public async Task<IActionResult> Attendance(AttendanceModel model)
        {
            var currentAttendace = await _context.Attendances
                .Where(a => a.GroupTermId == model.GroupTerm.GroupTermId)
                .Where(a => a.StudentId == model.SelectedStudentId)
                .SingleOrDefaultAsync();

            if (currentAttendace != null)
            {
                _context.Attendances.Remove(currentAttendace);
            }

            if (model.ToState != 0)
            {
                bool isLate = false;
                if (model.ToState == 1)
                    isLate = true;

                var newAttendace = new Attendance
                {
                    GroupTermId = model.GroupTerm.GroupTermId,
                    StudentId = (int)model.SelectedStudentId,
                    IsLate = isLate
                };

                _context.Attendances.Add(newAttendace);
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Attendance), model.GroupTerm.GroupTermId);
        }

        private bool GroupExists(int id)
        {
            return _context.Groups.Any(e => e.GroupId == id);
        }
    }
}
