﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PPZ.Data;

namespace PPZ.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupTermsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public GroupTermsController(ApplicationDbContext context)
        {
            _context = context;

        }
        // GET: api/GroupTerm
        [HttpGet]
        public async Task<List<GroupTerm>> Get()
        {
            return await _context.GroupTerms.ToListAsync();
        }

        // GET: api/GroupTerm/5
        [HttpGet("{id}", Name = "Get")]
        public async Task<List<GroupTerm>> Get(int? id)
        {
                                                        // GroupId != GroupTermsId
            return await _context.GroupTerms.Where(gT => gT.GroupId == id).ToListAsync();
        }

    }
}
