﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Students
    {
        public Students()
        {
            AccessKeys = new HashSet<AccessKeys>();
            Attendances = new HashSet<Attendances>();
            GroupStudents = new HashSet<GroupStudents>();
            Rfidmap = new HashSet<Rfidmap>();
            StudentGuardians = new HashSet<StudentGuardians>();
        }

        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public string Info { get; set; }
        public bool IsActive { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual ICollection<AccessKeys> AccessKeys { get; set; }
        public virtual ICollection<Attendances> Attendances { get; set; }
        public virtual ICollection<GroupStudents> GroupStudents { get; set; }
        public virtual ICollection<Rfidmap> Rfidmap { get; set; }
        public virtual ICollection<StudentGuardians> StudentGuardians { get; set; }
    }
}
