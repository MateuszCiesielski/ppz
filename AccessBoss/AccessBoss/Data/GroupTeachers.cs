﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class GroupTeachers
    {
        public int GroupTeacherId { get; set; }
        public int GroupId { get; set; }
        public int TeacherId { get; set; }

        public virtual Groups Group { get; set; }
        public virtual Teachers Teacher { get; set; }
    }
}
