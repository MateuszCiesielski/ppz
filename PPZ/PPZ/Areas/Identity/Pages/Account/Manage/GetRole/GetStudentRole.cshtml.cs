using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PPZ.Data;

namespace PPZ.Areas.Identity.Pages.Account.Manage
{
    public class GetStudentRoleModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public GetStudentRoleModel(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [BindProperty]
        [Display(Name = "Access key")]
        public string AccessKey { get; set; }

        [BindProperty]
        public Student Student { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        public IActionResult OnGet()
        {
            Student = null;
            return Page();
        }

        public async Task<IActionResult> OnPostKeyAsync()
        {
            Student = null;
            if (ModelState["AccessKey"] == null || (ModelState["AccessKey"]) != null && ModelState["AccessKey"].Errors.Any())
            {
                return Page();
            }
            var key = await _context.AccessKeys
                .Where(k => k.Key == AccessKey)
                .Where(k => k.Guardian == false)
                .SingleOrDefaultAsync();
            if (key == null)
            {
                ModelState.AddModelError("", "Invalid access key");
                return Page();
            }
            Student = await _context.Students
                .Where(s => s.StudentId == key.StudentId)
                .SingleOrDefaultAsync();
            return Page();
        }

        public async Task<IActionResult> OnPostStudentAsync()
        {
            if (Student == null)
            {
                return Page();
            }
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                Student = null;
                return Page();
            }
            user.StudentId = Student.StudentId;
            _context.Update(user);
            var key = await _context.AccessKeys
                .Where(k => k.Key == AccessKey)
                .Where(k => k.Guardian == false)
                .SingleOrDefaultAsync();
            if (key != null)
            {
                _context.Remove(key);
            }
            await _userManager.AddToRoleAsync(user, "Student");
            await _context.SaveChangesAsync();
            StatusMessage = "You are a student!";
            await _signInManager.RefreshSignInAsync(user);
            return RedirectToPage("../Index");
        }
    }
}
