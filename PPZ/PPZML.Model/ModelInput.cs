// This file was auto-generated by ML.NET Model Builder. 

using Microsoft.ML.Data;

namespace PPZML.Model
{
    public class ModelInput
    {
        [ColumnName("Spam"), LoadColumn(0)]
        public bool Spam { get; set; }


        [ColumnName("Message"), LoadColumn(1)]
        public string Message { get; set; }


    }
}
