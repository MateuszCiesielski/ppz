﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class Attendance
    {
        public int AttendanceId { get; set; }
        public int GroupTermId { get; set; }
        public int StudentId { get; set; }
        public bool IsLate { get; set; }
        public virtual GroupTerm GroupTerms { get; set; }
        public virtual Student Student { get; set; }
    }
}
