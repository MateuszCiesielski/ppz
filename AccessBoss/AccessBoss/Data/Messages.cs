﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Messages
    {
        public int MessageId { get; set; }
        public DateTime Time { get; set; }
        public string SenderUserId { get; set; }
        public string ReceiverUserId { get; set; }
        public string Content { get; set; }
        public bool IsViewed { get; set; }
        public bool IsDeletedBySender { get; set; }
        public bool IsDeletedByReceiver { get; set; }

        public virtual AspNetUsers ReceiverUser { get; set; }
        public virtual AspNetUsers SenderUser { get; set; }
    }
}
