﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Admin.Models
{
    public class RFIDMapCsvMap : ClassMap<PPZ.Data.RFIDMap>
    {
        public RFIDMapCsvMap()
        {
            Map(m => m.RFIDMapId).Optional();
            Map(m => m.KeyId);
            Map(m => m.StudentId);
        }
    }
}
