﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace PPZ.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Message>()
                .HasOne(o => o.ReceiverUser)
                .WithMany(m => m.MessagesAsReceiver);

            builder.Entity<Message>()
                .HasOne(o => o.SenderUser)
                .WithMany(m => m.MessagesAsSender);
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Guardian> Guardians { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<StudentGuardian> StudentGuardians { get; set; }
        public DbSet<GroupStudent> GroupStudents { get; set; }
        public DbSet<GroupTeacher> GroupTeachers { get; set; }
        public DbSet<AccessKey> AccessKeys { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Theme> Themes { get; set; }
        public DbSet<GroupTerm> GroupTerms { get; set; }
        public DbSet<Attendance> Attendances { get; set; }
        public DbSet<RFIDMap> RFIDMap { get; set; }
        public DbSet<Message> Messages { get; set; }
    }
}
