﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class GroupTeacher
    {
        public int GroupTeacherId { get; set; }
        public int GroupId { get; set; }
        public int TeacherId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Teacher Teacher { get; set; }
    }
}
