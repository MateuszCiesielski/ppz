﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Admin.Models
{
    public class StudentCsvMap : ClassMap<PPZ.Data.Student>
    {
        public StudentCsvMap()
        {
            Map(m => m.StudentId).Optional();
            Map(m => m.FirstName);
            Map(m => m.SecondName).Optional();
            Map(m => m.LastName);
            Map(m => m.FullName).Optional();
            Map(m => m.DateOfBirth);
            Map(m => m.Address).Optional();
            Map(m => m.EmailAddress).Optional();
            Map(m => m.PhoneNumber).Optional();
            Map(m => m.Height).Optional();
            Map(m => m.Weight).Optional();
            Map(m => m.Info).Optional();
            Map(m => m.IsActive).Optional();

        }
    }
}
