﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Students.Models;
using PPZ.Data;

namespace PPZ.Areas.Students.Controllers
{
    [Area("Student")]
    [Authorize(Roles = "Student")]    
    public class TeachersController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public TeachersController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Admin/Teachers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teachers
                .FirstOrDefaultAsync(m => m.TeacherId == id);

            if (teacher == null)
            {
                return NotFound();
            }

            var groups = await _context.Groups
                .Join(_context.GroupTeachers, g => g.GroupId, gs => gs.GroupId, (g, gs) => new { g, gs })
                .Where(ggs => ggs.gs.TeacherId == id)
                .Select(ggs => ggs.g)
                .ToListAsync();

            return View(new TeacherModel
            {
                Teacher = teacher,
                Groups = groups
            });
        }

    }
}
