﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class TeacherCsvMap : ClassMap<PPZ.Data.Teacher>
    {
        public TeacherCsvMap()
        {
            Map(m => m.TeacherId).Optional();
            Map(m => m.Prefix).Optional();
            Map(m => m.FirstName);
            Map(m => m.LastName);
            Map(m => m.FullName).Optional();
            Map(m => m.EmailAddress).Optional();
            Map(m => m.PhoneNumber).Optional();
            Map(m => m.Info).Optional();
            Map(m => m.Approved);
        }
    }
}
