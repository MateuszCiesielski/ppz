using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PPZ.Data;

namespace PPZ.Areas.Identity.Pages.Account.Manage
{
    public class AvatarModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public AvatarModel(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        [Display(Name = "Avatar")]
        public IFormFile FormFile { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                ApplicationUser currentUser = await _userManager.GetUserAsync(User);
                string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "images");

                if (currentUser.AvatarPath != null)
                {
                    string avatarToDeletePath = Path.Combine(uploadsFolder, currentUser.AvatarPath);

                    System.IO.File.Delete(avatarToDeletePath);
                }

                string uniqueFileName = null;
                if (FormFile != null)
                {
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + FormFile.FileName;
                    string filePath = Path.Combine(uploadsFolder, uniqueFileName);
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await FormFile.CopyToAsync(fileStream);
                    }
                }

                currentUser.AvatarPath = uniqueFileName;

                _context.ApplicationUsers.Update(currentUser);
                await _context.SaveChangesAsync();

            }

            return Page();

        }
    }
}
