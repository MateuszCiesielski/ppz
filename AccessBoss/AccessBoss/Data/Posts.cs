﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Posts
    {
        public int PostId { get; set; }
        public string ApplicationUserId { get; set; }
        public string Message { get; set; }
        public DateTime DateTime { get; set; }

        public virtual AspNetUsers ApplicationUser { get; set; }
    }
}
