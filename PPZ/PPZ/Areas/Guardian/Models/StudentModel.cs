﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Guardians.Models
{
    public class StudentModel
    {
        public Student Student { get; set; }
        public List<Guardian> Guardians { get; set; }
        public List<Group> Groups { get; set; }
        public string StudentKey { get; set; }
        public string GuardianKey { get; set; }
    }
}
