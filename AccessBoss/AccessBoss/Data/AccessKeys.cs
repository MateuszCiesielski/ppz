﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class AccessKeys
    {
        public int AccessKeyId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Key { get; set; }
        public string KeyHash { get; set; }
        public int StudentId { get; set; }
        public bool Guardian { get; set; }

        public virtual Students Student { get; set; }
    }
}
