﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PPZ.Data
{
    public class Teacher
    {        
        public int TeacherId { get; set; }
        public string Prefix { get; set; }
        [Required]
        [Display(Name = "First name*")]        
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Last name*")]
        public string LastName { get; set; }
        [Display(Name = "Email address")]
        [DataType(DataType.EmailAddress)]
        public string FullName => (Prefix + " " + FirstName + " " + LastName).TrimStart();
        public string EmailAddress { get; set; }
        [Display(Name = "Phone number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        [Display(Name = "Additional informations")]
        [DataType(DataType.MultilineText)]
        public string Info { get; set; }
        public bool Approved { get; set; }        

        public virtual List<GroupTeacher> GroupTeachers { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
