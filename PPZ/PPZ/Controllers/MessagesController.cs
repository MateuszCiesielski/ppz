﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PPZ.Data;
using PPZ.Models;

namespace PPZ.Controllers
{
    public class MessagesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public MessagesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var userId = await _userManager.GetUserIdAsync(await _userManager.GetUserAsync(User));

            List<MessagesModel> messages = new List<MessagesModel>();

            var people = await _context.Messages
                .Join(_context.ApplicationUsers, m => m.SenderUserId, au => au.Id, (m, au) => new { m, au })
                .Where(mau => mau.m.ReceiverUserId == userId)
                .Select(mau => mau.au)
                .ToListAsync();

            people.AddRange(await _context.Messages
                .Join(_context.ApplicationUsers, m => m.ReceiverUserId, au => au.Id, (m, au) => new { m, au })
                .Where(mau => mau.m.SenderUserId == userId)
                .Select(mau => mau.au)
                .ToListAsync());

            foreach (var person in people.Distinct())
            {
                string name = "";
                if (person.StudentId != null)
                {
                    name = await _context.Students
                        .Where(s => s.StudentId == person.StudentId)
                        .Select(s => s.FullName)
                        .SingleOrDefaultAsync();
                }
                if (person.GuardianId != null)
                {
                    name = await _context.Guardians
                        .Where(s => s.GuardianId == person.GuardianId)
                        .Select(s => s.FullName)
                        .SingleOrDefaultAsync();
                }
                if (person.TeacherId != null)
                {
                    name = await _context.Teachers
                        .Where(s => s.TeacherId == person.TeacherId)
                        .Select(s => s.FullName)
                        .SingleOrDefaultAsync();
                }

                var messageFrom = await _context.Messages
                    .Where(m => m.ReceiverUserId == userId)
                    .Where(m => m.SenderUserId == person.Id)
                    .OrderByDescending(m => m.Time)
                    .FirstOrDefaultAsync();

                var messageTo = await _context.Messages
                    .Where(m => m.SenderUserId == userId)
                    .Where(m => m.ReceiverUserId == person.Id)
                    .OrderByDescending(m => m.Time)
                    .FirstOrDefaultAsync();

                messages.Add(new MessagesModel
                {
                    UserId = person.Id,
                    UserName = name,
                    Time = messageTo.Time,
                    ShortContent = messageTo.Content + "..."
                });

                /*if (messageFrom.Time > messageTo.Time)
                {
                    
                }
                else
                {
                    messages.Add(new MessagesModel
                    {
                        UserId = person.Id,
                        UserName = name,
                        Time = messageTo.Time,
                        ShortContent = messageTo.Content.Substring(0, 30) + "..."
                    });
                }*/
            }

            return View(messages);
        }

        public async Task<IActionResult> NewMessage()
        {
            AdminInfo adminInfo = null;
            List<GroupInfo> contacts = new List<GroupInfo>();

            if (User.IsInRole("Admin"))
            {
                var groups = await _context.Groups.ToListAsync();

                foreach (var group in groups)
                {
                    var teachers = await _context.Teachers
                        .Join(_context.GroupTeachers, t => t.TeacherId, gt => gt.TeacherId, (t, gt) => new { t, gt })
                        .Where(tgt => tgt.gt.GroupId == group.GroupId)
                        .Select(tgt => tgt.t)
                        .ToListAsync();

                    List<TeacherInfo> teacherInfos = new List<TeacherInfo>();

                    foreach (var teacher in teachers)
                    {
                        var teacherAccountId = await _context.ApplicationUsers
                            .Where(au => au.TeacherId == teacher.TeacherId)
                            .Select(au => au.Id)
                            .SingleOrDefaultAsync();

                        teacherInfos.Add(new TeacherInfo
                        {
                            Teacher = teacher,
                            AccountId = teacherAccountId,
                            Send = false
                        });
                    }

                    var students = await _context.Students
                        .Join(_context.GroupStudents, s => s.StudentId, gs => gs.StudentId, (s, gs) => new { s, gs })
                        .Where(sgs => sgs.gs.GroupId == group.GroupId)
                        .Select(sgs => sgs.s)
                        .ToListAsync();

                    List<StudentGuardianInfo> studentsAndGuardians = new List<StudentGuardianInfo>();

                    foreach (var student in students)
                    {
                        var studentAccountId = await _context.ApplicationUsers
                            .Where(au => au.StudentId == student.StudentId)
                            .Select(au => au.Id)
                            .SingleOrDefaultAsync();

                        var guardian = await _context.Guardians
                            .Join(_context.StudentGuardians, g => g.GuardianId, sg => sg.GuardianId, (g, sg) => new { g, sg })
                            .Where(gsg => gsg.sg.StudentId == student.StudentId)
                            .Select(gsg => gsg.g)
                            .FirstOrDefaultAsync();

                        string guardianAccountId = "";
                        if (guardian != null)
                        {
                            guardianAccountId = await _context.ApplicationUsers
                                .Where(au => au.GuardianId == guardian.GuardianId)
                                .Select(au => au.Id)
                                .SingleOrDefaultAsync();
                        }

                        studentsAndGuardians.Add(new StudentGuardianInfo
                        {
                            Student = student,
                            StudentAccountId = studentAccountId,
                            StudentSend = false,
                            Guardian = guardian,
                            GuardianAccountId = guardianAccountId,
                            GuardianSend = false
                        });
                    }

                    var groupInfo = new GroupInfo
                    {
                        Group = group,
                        Teachers = teacherInfos,
                        StudentsAndGuardians = studentsAndGuardians
                    };

                    contacts.Add(groupInfo);
                }
            }

            return View(new NewMessageModel
            {
                Admin = adminInfo,
                Contacts = contacts
            });
        }

        [HttpPost]
        public async Task<IActionResult> NewMessage(NewMessageModel model)
        {
            var userId = await _userManager.GetUserIdAsync(await _userManager.GetUserAsync(User));

            if (model.Admin != null)
            {
                _context.Messages.Add(new Message
                {
                    ReceiverUserId = model.Admin.AccountId,
                    SenderUserId = userId,
                    Content = model.Message,
                    IsDeletedByReceiver = false,
                    IsDeletedBySender = false,
                    IsViewed = false,
                    Time = DateTime.Now
                });
            }

            List<string> teachers = new List<string>();

            foreach (var item in model.Contacts)
            {
                var groupTeachers = item.Teachers
                    .Where(t => t.Send == true)
                    .Select(t => t.AccountId)
                    .ToList();
                teachers.AddRange(groupTeachers);
            }

            foreach (var teacher in teachers.Distinct())
            {
                _context.Messages.Add(new Message
                {
                    ReceiverUserId = teacher,
                    SenderUserId = userId,
                    Content = model.Message,
                    IsDeletedByReceiver = false,
                    IsDeletedBySender = false,
                    IsViewed = false,
                    Time = DateTime.Now
                });
            }

            List<string> students = new List<string>();

            foreach (var item in model.Contacts)
            {
                var groupStudents = item.StudentsAndGuardians
                    .Where(s => s.StudentSend == true)
                    .Select(s => s.StudentAccountId)
                    .ToList();
                students.AddRange(groupStudents);
            }

            foreach (var student in students.Distinct())
            {
                _context.Messages.Add(new Message
                {
                    ReceiverUserId = student,
                    SenderUserId = userId,
                    Content = model.Message,
                    IsDeletedByReceiver = false,
                    IsDeletedBySender = false,
                    IsViewed = false,
                    Time = DateTime.Now
                });
            }

            List<string> guardians = new List<string>();

            foreach (var item in model.Contacts)
            {
                var groupGuardians = item.StudentsAndGuardians
                    .Where(g => g.Guardian != null)
                    .Where(g => g.GuardianSend == true)
                    .Select(g => g.GuardianAccountId)
                    .ToList();
                guardians.AddRange(groupGuardians);
            }

            foreach (var guardian in guardians.Distinct())
            {
                _context.Messages.Add(new Message
                {
                    ReceiverUserId = guardian,
                    SenderUserId = userId,
                    Content = model.Message,
                    IsDeletedByReceiver = false,
                    IsDeletedBySender = false,
                    IsViewed = false,
                    Time = DateTime.Now
                });
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Conversation()
        {
            return View();
        }
    }
}
