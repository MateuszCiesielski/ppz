﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Guardians
    {
        public Guardians()
        {
            StudentGuardians = new HashSet<StudentGuardians>();
        }

        public int GuardianId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual ICollection<StudentGuardians> StudentGuardians { get; set; }
    }
}
