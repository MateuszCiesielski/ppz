﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class RFIDMap
    {
        public int RFIDMapId { get; set; }
        public string KeyId { get; set; }
        public int StudentId { get; set; }

        public virtual Student Student { get; set; }
    }
}
