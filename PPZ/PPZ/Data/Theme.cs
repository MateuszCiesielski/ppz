﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PPZ.Data
{
    public class Theme
    {
        public int ThemeId { get; set; }
        [Required]
        [Display(Name = "Theme name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Link to theme")]
        public string Path { get; set; }
    }
}
