﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Text;
using System.Threading;

namespace AccessBoss
{
    public class Led
    {
        private int pin4;
        private int pin23;
        private GpioController controller;

        public Led()
        {
            pin4 = 4;
            pin23 = 23;
            controller = new GpioController();
            controller.OpenPin(pin4, PinMode.Output);
            controller.OpenPin(pin23, PinMode.Output);
        }

        public void Granted()
        {
            controller.Write(pin4, PinValue.High);
            Thread.Sleep(4000);
            controller.Write(pin4, PinValue.Low);
        }

        public void Denied()
        {
            controller.Write(pin23, PinValue.High);
            Thread.Sleep(4000);
            controller.Write(pin23, PinValue.Low);
        }

        public void Dispose()
        {
            controller.ClosePin(pin4);
            controller.ClosePin(pin23);
            controller.Dispose();
        }
    }
}
