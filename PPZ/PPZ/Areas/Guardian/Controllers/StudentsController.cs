﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Guardians.Models;
using PPZ.Data;

namespace PPZ.Areas.Guardians.Controllers
{
    [Area("Guardian")]
    [Authorize(Roles = "Guardian")]
    public class StudentsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public StudentsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Guardian/Students
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var studentGuardian = await _context.StudentGuardians.Where(sG => sG.GuardianId == user.GuardianId).Include(sG => sG.Student).FirstOrDefaultAsync();
            List<Student> students = new List<Student>();
            students.Add(studentGuardian.Student);

            return View(students);
        }


        // GET: Guardian/Students/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (student == null)
            {
                return NotFound();
            }

            var guardians = await _context.Guardians
                .Join(_context.StudentGuardians, g => g.GuardianId, sg => sg.GuardianId, (g, sg) => new { g, sg })
                .Where(gsg => gsg.sg.StudentId == id)
                .Select(gsg => gsg.g)
                .ToListAsync();

            var groups = await _context.Groups
                .Join(_context.GroupStudents, g => g.GroupId, gs => gs.GroupId, (g, gs) => new { g, gs })
                .Where(ggs => ggs.gs.StudentId == id)
                .Select(ggs => ggs.g)
                .ToListAsync();

            var studentKey = await _context.AccessKeys
                .Where(k => k.StudentId == id)
                .Where(k => k.Guardian == false)
                .FirstOrDefaultAsync();
            string studentKeyString = "Click below to generate a new key";
            if (studentKey != null)
                studentKeyString = studentKey.Key;

            var guardianKey = await _context.AccessKeys
                .Where(k => k.StudentId == id)
                .Where(k => k.Guardian == true)
                .FirstOrDefaultAsync();
            string guardianKeyString = "Click below to generate a new key";
            if (guardianKey != null)
                guardianKeyString = guardianKey.Key;

            return View(new StudentModel
            { 
                Student = student,
                Guardians = guardians,
                Groups = groups,
                StudentKey = studentKeyString,
                GuardianKey = guardianKeyString
            });
        }

        // GET: Guardian/Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Guardian/Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentId,FirstName,SecondName,LastName,DateOfBirth,Address,EmailAddress,PhoneNumber,Height,Weight,Info")] Student student)
        {
            if (id != student.StudentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.StudentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        private bool StudentExists(int id)
        {
            return _context.Students.Any(e => e.StudentId == id);
        }

    }
}
