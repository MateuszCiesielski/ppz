﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class AttendanceModel
    {
        public GroupTerm GroupTerm { get; set; }
        [Display(Name = "Group's name")]
        public string GroupName { get; set; }
        // List of students and his attendances: 0 - absent, 1 - late, 2 - present
        public List<Tuple<Student, int>> StudentsAttendances { get; set; }
        
        public int? SelectedStudentId { get; set; }
        // Change to state: 0 - absent, 1 - late, 2 - present
        public int ToState { get; set; }
    }
}
