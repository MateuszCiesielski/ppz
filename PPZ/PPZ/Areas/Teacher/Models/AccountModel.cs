﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class AccountModel
    {
        public ApplicationUser ApplicationUser { get; set; }
        [Display(Name = "Student")]
        public string StudentName { get; set; }
        [Display(Name = "Guardian")]
        public string GuardianName { get; set; }
        [Display(Name = "Teacher")]
        public string TeacherName { get; set; }
        public bool IsTeacherApproved { get; set; }
    }
}
