﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Themes
    {
        public int ThemeId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
