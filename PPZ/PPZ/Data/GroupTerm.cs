﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class GroupTerm
    {
        public int GroupTermId { get; set; }
        public int GroupId { get; set; }
        [Display(Name = "Start Time")]
        public DateTime DateStart { get; set; }
        [Display(Name = "End Time")]
        public DateTime DateEnd { get; set; }
        public virtual Group Group { get; set; }
    }
}
