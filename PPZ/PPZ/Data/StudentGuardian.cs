﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class StudentGuardian
    {
        public int StudentGuardianId { get; set; }
        public int StudentId { get; set; }
        public int GuardianId { get; set; }

        public Student Student { get; set; }
        public Guardian Guardian { get; set; }
    }
}
