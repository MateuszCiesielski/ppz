﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Groups
    {
        public Groups()
        {
            GroupStudents = new HashSet<GroupStudents>();
            GroupTeachers = new HashSet<GroupTeachers>();
            GroupTerms = new HashSet<GroupTerms>();
        }

        public int GroupId { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public string Info { get; set; }

        public virtual ICollection<GroupStudents> GroupStudents { get; set; }
        public virtual ICollection<GroupTeachers> GroupTeachers { get; set; }
        public virtual ICollection<GroupTerms> GroupTerms { get; set; }
    }
}
