﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Admin.Models;
using PPZ.Data;

namespace PPZ.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class AccountsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Admin/Groups
        public async Task<IActionResult> Index()
        {
            var applicationUsers = await _context.ApplicationUsers.ToListAsync();            
            var accounts = await GetAccountsWithConnectionsAsync(applicationUsers);
            return View(accounts);
        }

        // GET: Admin/Accounts/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Admin/Accounts/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var user = await _userManager.FindByIdAsync(id);                
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Admin/Accounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {            
            var user = await _userManager.FindByIdAsync(id);
            await _userManager.DeleteAsync(user);                        
            return RedirectToAction(nameof(Index));
        }

        private async Task<List<AccountModel>> GetAccountsWithConnectionsAsync(List<ApplicationUser> applicationUsers)
        {
            List<AccountModel> accounts = new List<AccountModel>();

            foreach (var user in applicationUsers)
            {
                string studentName = "not connected";
                string guardianName = "not connected";
                string teacherName = "not connected";
                bool isTeacherApproved = true;

                if (user.StudentId != null)
                {
                    var student = await _context.Students.Where(s => s.StudentId == user.StudentId).FirstOrDefaultAsync();
                    studentName = student.FirstName + " " + student.LastName;
                }
                if (user.GuardianId != null)
                {
                    var guardian = await _context.Guardians.Where(s => s.GuardianId == user.GuardianId).FirstOrDefaultAsync();
                    guardianName = guardian.FirstName + " " + guardian.LastName;
                }
                if (user.TeacherId != null)
                {
                    var teacher = await _context.Teachers.Where(s => s.TeacherId == user.TeacherId).FirstOrDefaultAsync();

                    if (teacher.Approved)
                        teacherName = teacher.FirstName + " " + teacher.LastName;
                    else
                    {
                        teacherName = "waiting for approval...";
                        isTeacherApproved = false;
                    }                                          
                }

                accounts.Add(new AccountModel
                {
                    ApplicationUser = user,
                    StudentName = studentName,
                    GuardianName = guardianName,
                    TeacherName = teacherName,
                    IsTeacherApproved = isTeacherApproved
                });
            }

            return accounts;
        }
    }
}