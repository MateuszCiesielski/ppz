﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function renderCalendarForUser(model, calendarElId) {
    document.addEventListener("DOMContentLoaded", function () {
        var initialLocaleCode = "en";
        var events = [];
        var calendarEl = document.getElementById(calendarElId);
        console.log(model);

        var overrides = {
            plugins: ["interaction", "dayGrid", "timeGrid", "list"],
            header: {
                left: "prev,next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
            },
            //defaultDate: "2020-05-12",
            themeSystem: "bootstrap",
            locale: initialLocaleCode,
            buttonIcons: false, // show the prev/next text
            weekNumbers: true,
            weekNumberCalculation: "ISO",
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            events: events
        };

        var calendar = new FullCalendar.Calendar(calendarEl, overrides);

        var j = 0;
        for (let i = 0; i < model.length; i++) {
            fetch(`https://localhost:44309/api/GroupTerms/${model[i].groupId}`)
                .then(response => response.json())
                .then(groupTermsArray => {
                    console.log(groupTermsArray);

                    groupTermsArray.forEach(groupTerm => {
                        overrides.events.push({ title: model[i].name, start: groupTerm.dateStart, end: groupTerm.dateEnd });
                    });
                    console.log(events);
                })
                .finally(() => {
                    j++;
                    if (j == model.length) {
                        calendar = new FullCalendar.Calendar(calendarEl, overrides);
                        calendar.render();
                    }
                });
        }

        if (model.length == 0) {
            calendar.render();
            console.log(null);
        }
    });
}

function renderCalendarForGroup(calendarElId, groupId, groupName) {
    document.addEventListener("DOMContentLoaded", function () {
        var initialLocaleCode = "en";
        var calendarEl = document.getElementById(calendarElId);
        var events = [];

        fetch("https://localhost:44309/api/GroupTerms/" + groupId)
            .then(response => response.json())
            .then(groupTermsArray => {
                console.log(groupTermsArray);

                groupTermsArray.forEach(groupTerm => {
                    events.push({
                        title: groupName,
                        start: groupTerm.dateStart,
                        end: groupTerm.dateEnd,
                        url: "https://localhost:44309/Admin/Groups/Attendance/" + groupTerm.groupTermId
                    });
                });
                console.log(events);

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ["interaction", "dayGrid", "timeGrid", "list"],
                    header: {
                        left: "prev,next today",
                        center: "title",
                        right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
                    },
                    //defaultDate: "2020-05-12",
                    themeSystem: "bootstrap",
                    locale: initialLocaleCode,
                    buttonIcons: false, // show the prev/next text
                    weekNumbers: true,
                    weekNumberCalculation: "ISO",
                    navLinks: true, // can click day/week names to navigate views
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: events
                });

                calendar.render();
            });
    });
}

function renderCalendarNoEdit(calendarElId, groupId, groupName) {
    document.addEventListener("DOMContentLoaded", function () {
        var initialLocaleCode = "en";
        var calendarEl = document.getElementById(calendarElId);
        var events = [];

        fetch("https://localhost:44309/api/GroupTerms/" + groupId)
            .then(response => response.json())
            .then(groupTermsArray => {
                console.log(groupTermsArray);

                groupTermsArray.forEach(groupTerm => {
                    events.push({
                        title: groupName,
                        start: groupTerm.dateStart,
                        end: groupTerm.dateEnd
                    });
                });
                console.log(events);

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ["interaction", "dayGrid", "timeGrid", "list"],
                    header: {
                        left: "prev,next today",
                        center: "title",
                        right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
                    },
                    //defaultDate: "2020-05-12",
                    locale: initialLocaleCode,
                    buttonIcons: false, // show the prev/next text
                    weekNumbers: true,
                    weekNumberCalculation: "ISO",
                    navLinks: true, // can click day/week names to navigate views
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: events
                });

                calendar.render();
            });
    });
}

function renderCalendarForManagement(calendarElId, groupId, groupName) {
    document.addEventListener("DOMContentLoaded", function () {
        var initialLocaleCode = "en";
        var calendarEl = document.getElementById(calendarElId);
        var events = [];

        var eventClick = function (info) {
            $('.modal-title').html(info.event.title);
            $('.modal-body').html(info.event.start.toDateString() + '<hr />' +
                info.event.start.toLocaleTimeString() + ' - ' + info.event.end.toLocaleTimeString());
            $('#attendance-link').html('<a class="btn btn-info" href="/Admin/Groups/Attendance/' + info.event.id + '">Attendance list</a>');
            $('#term-id').html('<input type="hidden" data-val="true" data-val-required="The Action field is required." id="GroupTermId" name="GroupTermId" value=\'' + info.event.id + '\' />');
            $('#calendar-modal').modal('show');
        }

        fetch("https://localhost:44309/api/GroupTerms/" + groupId)
            .then(response => response.json())
            .then(groupTermsArray => {
                console.log(groupTermsArray);

                groupTermsArray.forEach(groupTerm => {
                    events.push({
                        id: groupTerm.groupTermId,
                        title: groupName,
                        start: groupTerm.dateStart,
                        end: groupTerm.dateEnd
                    });
                });
                console.log(events);

                var calendar = new FullCalendar.Calendar(calendarEl, {
                    plugins: ["interaction", "dayGrid", "timeGrid", "list"],
                    header: {
                        left: "prev,next today",
                        center: "title",
                        right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
                    },
                    //defaultDate: "2020-05-12",
                    themeSystem: "bootstrap",
                    locale: initialLocaleCode,
                    buttonIcons: false, // show the prev/next text
                    weekNumbers: true,
                    weekNumberCalculation: "ISO",
                    navLinks: true, // can click day/week names to navigate views
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: events,
                    eventClick: eventClick
                });

                calendar.render();
            });
    });
}