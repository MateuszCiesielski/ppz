﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class GroupStudents
    {
        public int GroupStudentId { get; set; }
        public int GroupId { get; set; }
        public int StudentId { get; set; }

        public virtual Groups Group { get; set; }
        public virtual Students Student { get; set; }
    }
}
