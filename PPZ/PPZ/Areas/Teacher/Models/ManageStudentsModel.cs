﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class ManageStudentsModel
    {
        public Group Group { get; set; }
        public List<Student> AssignedStudents { get; set; }
        public List<Student> UnassignedStudents { get; set; }
        public bool Assign { get; set; }
        public Student Student { get; set; }
    }
}
