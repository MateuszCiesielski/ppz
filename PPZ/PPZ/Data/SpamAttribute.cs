﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PPZML.Model;

namespace PPZ.Data
{
    public class SpamAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            ModelInput modelInput = new ModelInput() { Message = value.ToString() };
            ModelOutput modelOutput = ConsumeModel.Predict(modelInput);

            if (modelOutput.Prediction == true)
                return new ValidationResult("Your message was classified as a 'spam' by machine learning model");
            else
                return ValidationResult.Success;
        }
    }
}
