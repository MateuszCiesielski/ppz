﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Students.Models;
using PPZ.Data;

namespace PPZ.Areas.Students.Controllers
{
    [Area("Student")]
    [Authorize(Roles = "Student")]
    public class GuardiansController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public GuardiansController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Admin/Guardians
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var studentGuardians = await _context.StudentGuardians.Where(sG => sG.StudentId == user.StudentId).Include(sG => sG.Guardian).ToListAsync();
            var guardians = new List<Guardian>();

            foreach(var studentGuardian in studentGuardians)
            {
                guardians.Add(studentGuardian.Guardian);
            }

            return View(guardians);
        }

  

        // GET: Admin/Guardians/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guardian = await _context.Guardians
                .FirstOrDefaultAsync(m => m.GuardianId == id);
            if (guardian == null)
            {
                return NotFound();
            }

            return View(guardian);
        }


    }
}
