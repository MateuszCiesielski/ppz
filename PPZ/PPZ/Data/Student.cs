﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PPZ.Data
{
    public class Student
    {
        public int StudentId { get; set; }
        [Required]
        [Display(Name = "First Name*")]
        public string FirstName { get; set; }
        [Display(Name = "Second Name")]
        public string SecondName { get; set; }
        [Required]
        [Display(Name = "Last Name*")]
        public string LastName { get; set; }
        public string FullName => FirstName + " " + LastName;
        [Required]
        [Display(Name = "Date of birth*")]
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }        
        public string Address { get; set; }
        [Display(Name = "Email address")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        [Display(Name = "Phone number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        [Display(Name = "Additional informations")]
        [DataType(DataType.MultilineText)]
        public string Info { get; set; }
        [Display(Name = "Status")]
        public bool IsActive { get; set; }

        public virtual List<GroupStudent> GroupStudents { get; set; }
        public virtual List<StudentGuardian> StudentGuardians { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
