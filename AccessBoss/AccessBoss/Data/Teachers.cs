﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Teachers
    {
        public Teachers()
        {
            GroupTeachers = new HashSet<GroupTeachers>();
        }

        public int TeacherId { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Info { get; set; }
        public bool Approved { get; set; }

        public virtual AspNetUsers AspNetUsers { get; set; }
        public virtual ICollection<GroupTeachers> GroupTeachers { get; set; }
    }
}
