﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class ManageTermsModel
    {
        public Group Group { get; set; }

        [Required]
        [DisplayName("Date")]
        [DataType(DataType.Date)]
        public DateTime? SingleDate { get; set; }

        [DisplayName("Time start")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:hh\:mm}")]
        public TimeSpan SingleTimeStart { get; set; }

        [DisplayName("Time end")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:hh\:mm}")]
        public TimeSpan SingleTimeEnd { get; set; }

        [Required]
        [DisplayName("From date")]
        [DataType(DataType.Date)]
        public DateTime? RangeDateStart { get; set; }

        [Required]
        [DisplayName("To date")]
        [DataType(DataType.Date)]
        public DateTime? RangeDateEnd { get; set; }

        [DisplayName("Day of week")]
        public DayOfWeek RangeDayOfWeek { get; set; }

        [DisplayName("Time start")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:hh\:mm}")]
        public TimeSpan RangeTimeStart { get; set; }

        [DisplayName("Time end")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:hh\:mm}")]
        public TimeSpan RangeTimeEnd { get; set; }

        // 0 - add single date, 1 - add range date, 2 - remove date, 3 - remove all
        public int Action { get; set; }

        public int GroupTermId { get; set; }

        public List<GroupTerm> GroupTerms { get; set; }
    }
}
