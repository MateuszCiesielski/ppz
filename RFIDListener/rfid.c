#include <stdio.h>
#include <stdlib.h>
#include <nfc/nfc.h>
#include <unistd.h>
#include <string.h>
 
 char uID[20] ="";
 char Last_uID[20] =" ";
 
 
static void SaveToFile(const uint8_t *pbtData, const size_t szBytes){
	size_t  szPos;
	  if (strcmp(Last_uID,uID)!=0) {
		 
			FILE *f = fopen("/home/pi/libnfctrial/data.txt", "w");
			if (f == NULL){
				printf("Error opening file!\n");
				exit(1);
			}
			fprintf(f,"ID: ");
	for (szPos = 0; szPos < szBytes; szPos++) {
		fprintf(f,"%x  ", pbtData[szPos]);
  }	  
			  fclose(f);
		
	  sprintf(Last_uID,"%s",uID);
	  }
}
 

static void print_hex2(const uint8_t *pbtData, const size_t szBytes)
{
  size_t  szPos;

  for (szPos = 0; szPos < szBytes; szPos++) {
    printf("%x  ", pbtData[szPos]);
  }
  printf("\n");
}
 
///
/// MAIN
///
int main(int argc, const char *argv[]) {
	
	nfc_device *pnd;
	nfc_target nt;
 
	// Allocate only a pointer to nfc_context
	nfc_context *context;
 
	// Initialize libnfc and set the nfc_context
	nfc_init(&context);
	if (context == NULL) {
	printf("      Unable to init libnfc (malloc)\n");
	exit(EXIT_FAILURE);
	}
 
 
	
	pnd = nfc_open(context, NULL);
 
	if (pnd == NULL) {
	printf("ERROR: %s\n", "Unable to open NFC device.");
	exit(EXIT_FAILURE);
	}
	// Set opened NFC device to initiator mode
	if (nfc_initiator_init(pnd) < 0) {
	nfc_perror(pnd, "nfc_initiator_init");
	exit(EXIT_FAILURE);
	}
 
	printf("NFC reader: %s opened\n", nfc_device_get_name(pnd));
 
 
 
	const uint8_t uiPollNr = 0x01; 
	const uint8_t uiPeriod = 0x07;   
	const size_t szModulations = 1;
	int res = 0;	
        const nfc_modulation nmModulations[1] = {  
						{ .nmt = NMT_ISO14443A, .nbr = NBR_106 }
						};
																	 
																	 
  
	while (1) {
		
			sprintf(uID,""); 
		 
			if ((res = nfc_initiator_poll_target(pnd, nmModulations, szModulations, uiPollNr, uiPeriod, &nt))  < 0) {  
				
			}
				
			
			if (res > 0) {		 
					printf("Tag  ID : ");
					print_hex2(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);  
					SaveToFile(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);  
					printf("Waiting for card removing "); 
					fflush(stdout);
					
					   while (0 == nfc_initiator_target_is_present(pnd, NULL)) {}
						nfc_perror(pnd, "...");
						sprintf(uID,""); 
						SaveToFile(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
						
					} else {
					  printf("Tag not found.\n");  
                                          SaveToFile(nt.nti.nai.abtUid, nt.nti.nai.szUidLen);
			                }
	 } 
	 
 	 
  // Close NFC device
  nfc_close(pnd);
  // Release the context
  nfc_exit(context);
  exit(EXIT_SUCCESS);
}
