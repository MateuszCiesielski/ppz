using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using PPZ.Data;

namespace PPZ.Areas.Identity.Pages.Account.Manage
{
    public class GetTeacherRoleModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public GetTeacherRoleModel(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public Teacher Teacher { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            string email = null;
            var user = await _userManager.GetUserAsync(User);
            if (user != null)
            {
                if (user.TeacherId != null)
                {
                    return RedirectToPage("../Index");
                }
                email = await _userManager.GetEmailAsync(user);
            }            
            if (email != null)
            {
                Teacher = new Teacher();
                Teacher.EmailAddress = email;
            }            

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Teacher.Approved = false;
            _context.Add(Teacher);
            await _context.SaveChangesAsync();

            var user = await _userManager.GetUserAsync(User);

            if (user == null)
            {
                return RedirectToPage();
            }

            user.TeacherId = Teacher.TeacherId;
            _context.Entry(user).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            await _context.SaveChangesAsync();

            StatusMessage = "Your form has been sent. Please wait for an administrator approval.";
            return RedirectToPage("../Index");
        }
    }
}
