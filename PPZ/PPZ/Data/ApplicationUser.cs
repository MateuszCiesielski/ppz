﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class ApplicationUser : IdentityUser
    {
        public int? TeacherId { get; set; }
        public int? StudentId { get; set; }
        public int? GuardianId { get; set; }
        public string AvatarPath { get; set; }
        public string ThemePath { get; set; }

        public virtual Teacher Teacher { get; set; }
        public virtual Student Student { get; set; }
        public virtual Guardian Guardian { get; set; }
        public virtual List<Message> MessagesAsSender { get; set; }
        public virtual List<Message> MessagesAsReceiver { get; set; }
    }
}
