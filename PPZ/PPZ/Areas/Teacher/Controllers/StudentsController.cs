﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Teachers.Models;
using PPZ.Data;

namespace PPZ.Areas.Teachers.Controllers
{
    [Area("Teacher")]
    [Authorize(Roles = "Teacher")]
    public class StudentsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public StudentsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Teacher/Students
        public async Task<IActionResult> Index()
        {
            return View(await _context.Students.ToListAsync());                  
        }

        public IActionResult ImportCsv()
        {
            CsvModel csvModel = new CsvModel();
            return View(csvModel);
        }

        [HttpPost]
        public async Task<IActionResult> ImportCsv(CsvModel csvModel)
        {

            string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "csv");
            string fileName = csvModel.FormFile.FileName;
            string filePath = Path.Combine(uploadsFolder,fileName);

            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await csvModel.FormFile.CopyToAsync(fileStream);
            }

            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.RegisterClassMap<StudentCsvMap>();
                var students = csv.GetRecords<Student>();

                try
                {
                    await _context.AddRangeAsync(students);
                    await _context.SaveChangesAsync();
                }
                catch(Exception e)
                {
                    var model = new CsvErrorPageModel() { ErrorMessage = e.Message, FilePath = filePath };
                    return View("../CsvErrorPage", model);
                }
            }

            System.IO.File.Delete(filePath);

            return RedirectToAction(nameof(Index));
        }

        // GET: Teacher/Students/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (student == null)
            {
                return NotFound();
            }

            var guardians = await _context.Guardians
                .Join(_context.StudentGuardians, g => g.GuardianId, sg => sg.GuardianId, (g, sg) => new { g, sg })
                .Where(gsg => gsg.sg.StudentId == id)
                .Select(gsg => gsg.g)
                .ToListAsync();

            var groups = await _context.Groups
                .Join(_context.GroupStudents, g => g.GroupId, gs => gs.GroupId, (g, gs) => new { g, gs })
                .Where(ggs => ggs.gs.StudentId == id)
                .Select(ggs => ggs.g)
                .ToListAsync();

            var studentKey = await _context.AccessKeys
                .Where(k => k.StudentId == id)
                .Where(k => k.Guardian == false)
                .FirstOrDefaultAsync();
            string studentKeyString = "Click below to generate a new key";
            if (studentKey != null)
                studentKeyString = studentKey.Key;

            var guardianKey = await _context.AccessKeys
                .Where(k => k.StudentId == id)
                .Where(k => k.Guardian == true)
                .FirstOrDefaultAsync();
            string guardianKeyString = "Click below to generate a new key";
            if (guardianKey != null)
                guardianKeyString = guardianKey.Key;

            return View(new StudentModel
            { 
                Student = student,
                Guardians = guardians,
                Groups = groups,
                StudentKey = studentKeyString,
                GuardianKey = guardianKeyString
            });
        }

        // GET: Teacher/Students/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Teacher/Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentId,FirstName,SecondName,LastName,DateOfBirth,Address,EmailAddress,PhoneNumber,Height,Weight,Info,IsActive")] Student student)
        {
            if (ModelState.IsValid)
            {
                _context.Add(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Teacher/Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students.FindAsync(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Teacher/Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentId,FirstName,SecondName,LastName,DateOfBirth,Address,EmailAddress,PhoneNumber,Height,Weight,Info,IsActive")] Student student)
        {
            if (id != student.StudentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.StudentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        // GET: Teacher/Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Teacher/Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _context.Students.FindAsync(id);
            var user = await _context.ApplicationUsers.Where(u => u.StudentId == id).SingleOrDefaultAsync();
            if (user != null)
            {
                await _userManager.RemoveFromRoleAsync(user, "Student");
                user.StudentId = null;
                _context.Update(user);
            }
            var studentGuardians = await _context.StudentGuardians.Where(sg => sg.StudentId == id).ToListAsync();
            _context.StudentGuardians.RemoveRange(studentGuardians);
            var groupStudents = await _context.GroupStudents.Where(gs => gs.StudentId == id).ToListAsync();
            _context.GroupStudents.RemoveRange(groupStudents);
            var accessKeys = await _context.AccessKeys.Where(a => a.StudentId == id).SingleOrDefaultAsync();
            _context.AccessKeys.Remove(accessKeys);
            var attendances = await _context.Attendances.Where(a => a.StudentId == id).ToListAsync();
            _context.Attendances.RemoveRange(attendances);
            _context.Students.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // POST: Teacher/Students/GenerateKeyForStudent/5        
        public async Task<IActionResult> GenerateKeyForStudent(int id)
        {
            var oldKey = await _context.AccessKeys
                .Where(k => k.Guardian == false)
                .Where(k => k.StudentId == id).SingleOrDefaultAsync();
            if (oldKey != null)
            {
                _context.Remove(oldKey);
            }
            string genKey;
            AccessKey duplicate;
            do
            {
                genKey = GenerateRandomString(8);
                duplicate = await _context.AccessKeys.Where(k => k.Key == genKey).SingleOrDefaultAsync();
            } while (duplicate != null);
            AccessKey accessKey = new AccessKey
            {
                DateCreated = DateTime.Now,
                Key = genKey,
                KeyHash = SharedFunctions.GetSHA256FromString(genKey),
                StudentId = id,
                Guardian = false
            };
            _context.AccessKeys.Add(accessKey);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new { id = id });
        }

        // POST: Teacher/Students/GenerateKeyForGuardian/5        
        public async Task<IActionResult> GenerateKeyForGuardian(int id)
        {
            var oldKey = await _context.AccessKeys
                .Where(k => k.Guardian == true)
                .Where(k => k.StudentId == id).SingleOrDefaultAsync();
            if (oldKey != null)
            {
                _context.Remove(oldKey);
            }
            string genKey;
            AccessKey duplicate;
            do
            {
                genKey = GenerateRandomString(8);
                duplicate = await _context.AccessKeys.Where(k => k.Key == genKey).SingleOrDefaultAsync();
            } while (duplicate != null);
            AccessKey accessKey = new AccessKey
            {
                DateCreated = DateTime.Now,
                Key = genKey,
                KeyHash = SharedFunctions.GetSHA256FromString(genKey),
                StudentId = id,
                Guardian = true
            };
            _context.AccessKeys.Add(accessKey);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Details), new { id = id });
        }

        private bool StudentExists(int id)
        {
            return _context.Students.Any(e => e.StudentId == id);
        }

        private string GenerateRandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
