﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PPZ.Data;
using PPZ.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace PPZ.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, ILogger<HomeController> logger)
        {
            _context = context;
            _logger = logger;
            _userManager = userManager;
        }


        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.user = user;
            var posts = _context.Posts.Include(p => p.ApplicationUser).ToList();
            return View(posts);
        }

        public async Task<IActionResult> Calendar()
        {
            var user = await _userManager.GetUserAsync(User);
            List<int> groupIdList = new List<int>();
            List<Group> groupList = new List<Group>();

            if (user != null)
            {
                if (user.StudentId != null)
                    groupIdList = await _context.GroupStudents.Where(gS => gS.StudentId == user.StudentId).Select(gS => gS.GroupId).ToListAsync();
                else if (user.GuardianId != null)
                {
                    var studentGuardian = await _context.StudentGuardians.Where(sG => sG.GuardianId == user.GuardianId).Include(sG => sG.Student).FirstOrDefaultAsync();                   
                    groupIdList = await _context.GroupStudents.Where(gS => gS.StudentId == studentGuardian.StudentId).Select(gS => gS.GroupId).ToListAsync();

                }
                else if (user.TeacherId != null)
                    groupIdList = await _context.GroupTeachers.Where(gS => gS.TeacherId == user.TeacherId).Select(gS => gS.GroupId).ToListAsync();

                for (int i = 0; i < groupIdList.Count; i++)
                    groupList.Add(await _context.Groups.Where(g => g.GroupId == groupIdList[i]).FirstOrDefaultAsync());
            }

            return View(groupList);
        }

        public IActionResult CreatePost()
        {       
            Post post = new Post();
            return View(post);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePost(Post post)
        {
            if (!ModelState.IsValid)
                return View();
                //return Content("ModelState isn't valid");

            post.ApplicationUser = await _userManager.GetUserAsync(User);
            post.DateTime = DateTime.Now;

            _context.Posts.Add(post);

            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Posts
                .SingleOrDefaultAsync(p => p.PostId == id);

            if (post == null)
            {
                return NotFound();
            }

            _context.Remove(post);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index", "Home");
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
