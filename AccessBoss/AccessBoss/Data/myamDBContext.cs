﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ModuleListener.Data
{
    public partial class myamDBContext : DbContext
    {
        public myamDBContext()
        {
        }

        public myamDBContext(DbContextOptions<myamDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccessKeys> AccessKeys { get; set; }
        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Attendances> Attendances { get; set; }
        public virtual DbSet<GroupStudents> GroupStudents { get; set; }
        public virtual DbSet<GroupTeachers> GroupTeachers { get; set; }
        public virtual DbSet<GroupTerms> GroupTerms { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<Guardians> Guardians { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<Posts> Posts { get; set; }
        public virtual DbSet<Rfidmap> Rfidmap { get; set; }
        public virtual DbSet<StudentGuardians> StudentGuardians { get; set; }
        public virtual DbSet<Students> Students { get; set; }
        public virtual DbSet<Teachers> Teachers { get; set; }
        public virtual DbSet<Themes> Themes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=51.68.136.253;Port=5432;Database=myamDB;Username=myamDBuser;Password=zaq1@WSX");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccessKeys>(entity =>
            {
                entity.HasKey(e => e.AccessKeyId);

                entity.HasIndex(e => e.StudentId);

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.AccessKeys)
                    .HasForeignKey(d => d.StudentId);
            });

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.GuardianId)
                    .IsUnique();

                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique();

                entity.HasIndex(e => e.StudentId)
                    .IsUnique();

                entity.HasIndex(e => e.TeacherId)
                    .IsUnique();

                entity.Property(e => e.Discriminator)
                    .IsRequired()
                    .HasDefaultValueSql("''::text");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.LockoutEnd).HasColumnType("timestamp with time zone");

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);

                entity.HasOne(d => d.Guardian)
                    .WithOne(p => p.AspNetUsers)
                    .HasForeignKey<AspNetUsers>(d => d.GuardianId);

                entity.HasOne(d => d.Student)
                    .WithOne(p => p.AspNetUsers)
                    .HasForeignKey<AspNetUsers>(d => d.StudentId);

                entity.HasOne(d => d.Teacher)
                    .WithOne(p => p.AspNetUsers)
                    .HasForeignKey<AspNetUsers>(d => d.TeacherId);
            });

            modelBuilder.Entity<Attendances>(entity =>
            {
                entity.HasKey(e => e.AttendanceId);

                entity.HasIndex(e => e.GroupTermId);

                entity.HasIndex(e => e.StudentId);

                entity.HasOne(d => d.GroupTerm)
                    .WithMany(p => p.Attendances)
                    .HasForeignKey(d => d.GroupTermId);

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Attendances)
                    .HasForeignKey(d => d.StudentId);
            });

            modelBuilder.Entity<GroupStudents>(entity =>
            {
                entity.HasKey(e => e.GroupStudentId);

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.StudentId);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupStudents)
                    .HasForeignKey(d => d.GroupId);

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.GroupStudents)
                    .HasForeignKey(d => d.StudentId);
            });

            modelBuilder.Entity<GroupTeachers>(entity =>
            {
                entity.HasKey(e => e.GroupTeacherId);

                entity.HasIndex(e => e.GroupId);

                entity.HasIndex(e => e.TeacherId);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupTeachers)
                    .HasForeignKey(d => d.GroupId);

                entity.HasOne(d => d.Teacher)
                    .WithMany(p => p.GroupTeachers)
                    .HasForeignKey(d => d.TeacherId);
            });

            modelBuilder.Entity<GroupTerms>(entity =>
            {
                entity.HasKey(e => e.GroupTermId);

                entity.HasIndex(e => e.GroupId);

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupTerms)
                    .HasForeignKey(d => d.GroupId);
            });

            modelBuilder.Entity<Groups>(entity =>
            {
                entity.HasKey(e => e.GroupId);

                entity.Property(e => e.Name).IsRequired();
            });

            modelBuilder.Entity<Guardians>(entity =>
            {
                entity.HasKey(e => e.GuardianId);

                entity.Property(e => e.FirstName).IsRequired();

                entity.Property(e => e.LastName).IsRequired();
            });

            modelBuilder.Entity<Messages>(entity =>
            {
                entity.HasKey(e => e.MessageId);

                entity.HasIndex(e => e.ReceiverUserId);

                entity.HasIndex(e => e.SenderUserId);

                entity.HasOne(d => d.ReceiverUser)
                    .WithMany(p => p.MessagesReceiverUser)
                    .HasForeignKey(d => d.ReceiverUserId);

                entity.HasOne(d => d.SenderUser)
                    .WithMany(p => p.MessagesSenderUser)
                    .HasForeignKey(d => d.SenderUserId);
            });

            modelBuilder.Entity<Posts>(entity =>
            {
                entity.HasKey(e => e.PostId);

                entity.HasIndex(e => e.ApplicationUserId);

                entity.HasOne(d => d.ApplicationUser)
                    .WithMany(p => p.Posts)
                    .HasForeignKey(d => d.ApplicationUserId);
            });

            modelBuilder.Entity<Rfidmap>(entity =>
            {
                entity.ToTable("RFIDMap");

                entity.HasIndex(e => e.StudentId);

                entity.Property(e => e.RfidmapId).HasColumnName("RFIDMapId");

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.Rfidmap)
                    .HasForeignKey(d => d.StudentId);
            });

            modelBuilder.Entity<StudentGuardians>(entity =>
            {
                entity.HasKey(e => e.StudentGuardianId);

                entity.HasIndex(e => e.GuardianId);

                entity.HasIndex(e => e.StudentId);

                entity.HasOne(d => d.Guardian)
                    .WithMany(p => p.StudentGuardians)
                    .HasForeignKey(d => d.GuardianId);

                entity.HasOne(d => d.Student)
                    .WithMany(p => p.StudentGuardians)
                    .HasForeignKey(d => d.StudentId);
            });

            modelBuilder.Entity<Students>(entity =>
            {
                entity.HasKey(e => e.StudentId);

                entity.Property(e => e.FirstName).IsRequired();

                entity.Property(e => e.LastName).IsRequired();
            });

            modelBuilder.Entity<Teachers>(entity =>
            {
                entity.HasKey(e => e.TeacherId);

                entity.Property(e => e.FirstName).IsRequired();

                entity.Property(e => e.LastName).IsRequired();
            });

            modelBuilder.Entity<Themes>(entity =>
            {
                entity.HasKey(e => e.ThemeId);

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Path).IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
