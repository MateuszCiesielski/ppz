﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class GuardianCsvMap : ClassMap<PPZ.Data.Guardian>
    {
        public GuardianCsvMap()
        {
            Map(m => m.GuardianId).Optional();
            Map(m => m.FirstName);
            Map(m => m.LastName);
            Map(m => m.FullName).Optional();
            Map(m => m.Address).Optional();
            Map(m => m.EmailAddress).Optional();
            Map(m => m.PhoneNumber).Optional();
        }
    }
}
