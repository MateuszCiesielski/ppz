﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class StudentGuardians
    {
        public int StudentGuardianId { get; set; }
        public int StudentId { get; set; }
        public int GuardianId { get; set; }

        public virtual Guardians Guardian { get; set; }
        public virtual Students Student { get; set; }
    }
}
