﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Admin.Models
{
    public class GroupCsvMap : ClassMap<PPZ.Data.Group>
    {
        public GroupCsvMap()
        {
            Map(m => m.GroupId).Optional();
            Map(m => m.Name);
            Map(m => m.Location).Optional();
            Map(m => m.Info).Optional();
            Map(m => m.Info).Optional();

        }
    }
}
