﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Teachers.Models
{
    public class CsvErrorPageModel
    {
        public string ErrorMessage { get; set; }
        public string FilePath { get; set; }
    }
}
