﻿using PPZ.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Areas.Guardians.Models
{
    public class TeacherModel
    {
        public Teacher Teacher { get; set; }
        public List<Group> Groups { get; set; }
    }
}
