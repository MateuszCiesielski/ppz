﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PPZ
{
    public static class SharedFunctions
    {
        public static string GetSHA256FromString(string text)
        {
            SHA256Managed sha = new SHA256Managed();
            StringBuilder hash = new StringBuilder();
            byte[] hashByte = sha.ComputeHash(Encoding.UTF8.GetBytes(text));
            foreach (var item in hashByte)
            {
                hash.Append(item.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}
