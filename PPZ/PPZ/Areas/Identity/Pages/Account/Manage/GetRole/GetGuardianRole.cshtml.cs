using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PPZ.Data;

namespace PPZ.Areas.Identity.Pages.Account.Manage
{
    public class GetGuardianRoleModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public GetGuardianRoleModel(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [BindProperty]
        [Display(Name = "Access key")]
        public string AccessKey { get; set; }

        [BindProperty]
        public int StudentId { get; set; }

        [BindProperty]
        public Guardian Guardian { get; set; }

        [BindProperty]
        public bool IsAccepted { get; set; }

        public Student Student { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        public IActionResult OnGet()
        {
            StudentId = -1;
            IsAccepted = false;
            return Page();
        }

        public async Task<IActionResult> OnPostKeyAsync()
        {
            IsAccepted = false;      
            var key = await _context.AccessKeys
                .Where(k => k.Key == AccessKey)
                .Where(k => k.Guardian == true)
                .SingleOrDefaultAsync();
            if (key == null)
            {
                ModelState.AddModelError("", "Invalid access key");
                return Page();
            }
            StudentId = key.StudentId;
            Student = await _context.Students.FindAsync(StudentId);
            return Page();
        }

        public IActionResult OnPostAccept()
        {
            IsAccepted = true;
            return Page();
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (StudentId == -1)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return Page();
            }

            _context.Add(Guardian);
            await _context.SaveChangesAsync();

            user.GuardianId = Guardian.GuardianId;
            _context.Update(user);

            var key = await _context.AccessKeys
                .Where(k => k.StudentId == StudentId)
                .Where(k => k.Guardian == true)
                .SingleOrDefaultAsync();
            if (key != null)
            {
                _context.Remove(key);
            }

            StudentGuardian studentGuardian = new StudentGuardian
            {
                GuardianId = Guardian.GuardianId,
                StudentId = StudentId
            };
            _context.Update(studentGuardian);

            await _userManager.AddToRoleAsync(user, "Guardian");
            await _context.SaveChangesAsync();
            StatusMessage = "You are a guardian!";
            await _signInManager.RefreshSignInAsync(user);
            return RedirectToPage("../Index");
        }
    }
}
