﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using PPZ.Data;

namespace PPZ.Areas.Identity.Pages.Account.Manage
{
    public class ThemeModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ThemeModel(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public string ThemeId { get; set; }
        
        [BindProperty]
        public List<SelectListItem> ThemeList { get; set; }
        public IActionResult OnGet()
        {
            ThemeList = _context.Themes.Select(a =>
                                          new SelectListItem
                                          {
                                              Value = a.ThemeId.ToString(),
                                              Text = a.Name
                                          }).ToList();
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                ApplicationUser currentUser = await _userManager.GetUserAsync(User);

                int id= Convert.ToInt32(ThemeId);
                
                currentUser.ThemePath = _context.Themes.Where(x => x.ThemeId == id).Select(x => x.Path).FirstOrDefault();
                _context.ApplicationUsers.Update(currentUser);
                await _context.SaveChangesAsync();
            }
            return RedirectToPage();
        }
    }
}
