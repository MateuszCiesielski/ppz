# MyAM
### Uruchomienie projektu:

1. Zainstalowanie bazy danych **POSTGRESQL** (w celu testu najprościej jest to zrobić na Windowsie)
    - [**[INSTALATOR]**](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads "POSTGRESQL"), który zainstaluje także PgAdmin do zarządzania bazą oraz Stack Builder do pobierania dodatkowych pakietów.
    - bazę zainstalować z domyślnymi ustawieniami, aby serwer stał na **localhoscie**, na porcie **5432**
    - ~~nie pamiętam, czy po stronie bazy trzeba zainstalować sterownik npgsql (sterownik POSTGRESQL dla .Net), ale jeśli później coś nie będzie działać, to należy zainstalować npgsql używając Stack Builder~~

2. W katalogu projektu PPZ jest plik **appsettings.json**, który ma zdefiniowanego connection stringa dla połączenia z bazą danych:
    - **"PpzDBConnection":  "Server=127.0.0.1;Port=5432;Database=ppzDB;Username=ppzDBaccess;Password=zaq1@WSX"**
    - zgodnie z tym connection stringiem w bazie danych używając PgAdmin, na domyślnym serwerze należy stworzyć bazę danych **ppzDB**
    - w **Login/Group Roles** trzeba stworzyć użytkownika **ppzDBaccess** z hasłem **zaq1@WSX** i ustawić mu opcję **CanLogin** na **TRUE**
    - we właściwościach bazy **ppzDB** ustawić dostęp dla użytkownika **ppzDBaccess** ze wszystkimi uprawnieniami

3. Uruchomienie projektu w Visual Studio:
    - Uruchomienie konsoli menadżera pakietów (Tools -> NuGet Package Manager -> Package Manager Console)
    - uruchomienie komendy **Update-Database**
    - Kompilacja projektu i uruchomienie

4. Dodatkowe info:
    - Są zdefiniowane 4 role: Admin, Teacher, Guardian, Student
    - Konto admina jest tworzone przy pierwszym uruchomieniu. Można się zalogować: email = **admin@myam.com**, password = **zaq1@WSX**
    - Można utworzyć sobie nowe konto bez ról

