﻿using System;
using System.Collections.Generic;

namespace ModuleListener.Data
{
    public partial class Attendances
    {
        public int AttendanceId { get; set; }
        public int GroupTermId { get; set; }
        public int StudentId { get; set; }
        public bool IsLate { get; set; }

        public virtual GroupTerms GroupTerm { get; set; }
        public virtual Students Student { get; set; }
    }
}
