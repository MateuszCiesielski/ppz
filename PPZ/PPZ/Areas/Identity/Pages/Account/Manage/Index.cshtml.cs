﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using PPZ.Data;

namespace PPZ.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public IndexModel(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;            
        }        

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public ApplicationUser ApplicationUser { get; set; }

        [BindProperty]
        public Student Student { get; set; }

        [BindProperty]
        public Guardian Guardian { get; set; }

        [BindProperty]
        public Teacher Teacher { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            ApplicationUser = await _userManager.GetUserAsync(User);
            if(ApplicationUser == null)
            {
                return NotFound();
            }
            if(ApplicationUser.StudentId != null)
            {
                Student = await _context.Students.Where(s => s.StudentId == ApplicationUser.StudentId).SingleOrDefaultAsync(); 
            }
            if(ApplicationUser.GuardianId != null)
            {
                Guardian = await _context.Guardians.Where(g => g.GuardianId == ApplicationUser.GuardianId).SingleOrDefaultAsync();
            }
            if(ApplicationUser.TeacherId != null)
            {
                Teacher = await _context.Teachers.Where(t => t.TeacherId == ApplicationUser.TeacherId).SingleOrDefaultAsync();
            }
            return Page();
        }
    }
}
