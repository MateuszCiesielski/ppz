﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Students.Models;
using PPZ.Data;

namespace PPZ.Areas.Students.Controllers
{
    [Area("Student")]
    [Authorize(Roles = "Student")]
    public class GroupsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public GroupsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Student/Groups
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var groupStudents = await _context.GroupStudents.Where(gS => gS.StudentId == user.StudentId).Include(gS => gS.Group).ToListAsync();
            var groups = new List<Group>();

            foreach (var groupStudent in groupStudents)
            {
                groups.Add(groupStudent.Group);
            }

            return View(groups);
        }

        // GET: Student/Groups/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @group = await _context.Groups
                .FirstOrDefaultAsync(m => m.GroupId == id);
            if (@group == null)
            {
                return NotFound();
            }

            var teachers = await _context.Teachers.ToListAsync();

            var groupTeachers = teachers
                .Join(_context.GroupTeachers, t => t.TeacherId, gt => gt.TeacherId, (t, gt) => new { t, gt })
                .Where(tgt => tgt.gt.GroupId == id)
                .Select(tgt => tgt.t)
                .ToList();

            var students = await _context.Students.ToListAsync();

            var groupStudents = students
                .Join(_context.GroupStudents, s => s.StudentId, gs => gs.StudentId, (s, gs) => new { s, gs })
                .Where(sgs => sgs.gs.GroupId == id)
                .Select(sgs => sgs.s)
                .ToList();

            return View(new GroupModel 
            { 
                Group = @group,
                GroupTeachers = groupTeachers,
                GroupStudents = groupStudents
            });
        }

    }
}
