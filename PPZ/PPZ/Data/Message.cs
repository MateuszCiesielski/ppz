﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PPZ.Data
{
    public class Message
    {
        public int MessageId { get; set; }
        public DateTime Time { get; set; }
        public string SenderUserId { get; set; }
        public string ReceiverUserId { get; set; }
        public string Content { get; set; }
        public bool IsViewed { get; set; }
        public bool IsDeletedBySender { get; set; }
        public bool IsDeletedByReceiver { get; set; }

        public virtual ApplicationUser SenderUser { get; set; }
        public virtual ApplicationUser ReceiverUser { get; set; }
    }
}
