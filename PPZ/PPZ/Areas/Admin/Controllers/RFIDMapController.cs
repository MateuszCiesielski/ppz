﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PPZ.Areas.Admin.Models;
using PPZ.Data;

namespace PPZ.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class RFIDMapController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public RFIDMapController(ApplicationDbContext context, IWebHostEnvironment webHostEnvironment)
        {
            _context = context;
            _webHostEnvironment = webHostEnvironment;
        }

        // GET: Admin/RFIDMap
        public async Task<IActionResult> Index()
        {
            var ids = await _context.RFIDMap.ToListAsync();
            List<Models.RFIDMapModel> model = new List<RFIDMapModel>();
            foreach (var id in ids)
            {
                var student = await _context.Students
                    .Where(s => s.StudentId == id.StudentId)
                    .SingleOrDefaultAsync();
                model.Add(new RFIDMapModel { rfidMap = id, Student = student });
            }
            return View(model);
        }

        public IActionResult ImportCsv()
        {
            CsvModel csvModel = new CsvModel();
            return View(csvModel);
        }

        [HttpPost]
        public async Task<IActionResult> ImportCsv(CsvModel csvModel)
        {

            string uploadsFolder = Path.Combine(_webHostEnvironment.WebRootPath, "csv");
            string fileName = csvModel.FormFile.FileName;
            string filePath = Path.Combine(uploadsFolder,fileName);

            using (FileStream fileStream = new FileStream(filePath, FileMode.Create))
            {
                await csvModel.FormFile.CopyToAsync(fileStream);
            }

            using (var reader = new StreamReader(filePath))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                csv.Configuration.RegisterClassMap<RFIDMapCsvMap>();
                var students = csv.GetRecords<RFIDMap>();

                try
                {
                    await _context.AddRangeAsync(students);
                    await _context.SaveChangesAsync();
                }
                catch(Exception e)
                {
                    var model = new CsvErrorPageModel() { ErrorMessage = e.Message, FilePath = filePath };
                    return View("../CsvErrorPage", model);
                }
            }

            System.IO.File.Delete(filePath);

            return RedirectToAction(nameof(Index));
        }

        // GET: Admin/RFIDMap/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rfidMap = await _context.RFIDMap
                .SingleOrDefaultAsync(m => m.RFIDMapId == id);
            if (rfidMap == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                    .Where(s => s.StudentId == rfidMap.StudentId)
                    .SingleOrDefaultAsync();

            return View(new RFIDMapModel { rfidMap = rfidMap, Student = student });
        }

        // GET: Admin/Students/Create
        public async Task<IActionResult> Create()
        {
            var students = await _context.Students.ToListAsync();
            return View(new RFIDMapModel { StudentList = students });
        }

        // POST: Admin/Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RFIDMapModel model)
        {
            if (ModelState.IsValid)
            {
                _context.Add(model.rfidMap);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(model.rfidMap);
        }

        // GET: Admin/Students/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rfidMap = await _context.RFIDMap.FindAsync(id);
            if (rfidMap == null)
            {
                return NotFound();
            }

            var students = await _context.Students.ToListAsync();

            return View(new RFIDMapModel { rfidMap = rfidMap, StudentList = students });
        }

        // POST: Admin/Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, RFIDMapModel model)
        {
            if (id != model.rfidMap.RFIDMapId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(model.rfidMap);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RFIDMapExists(model.rfidMap.RFIDMapId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(model.rfidMap);
        }

        // GET: Admin/Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rfidMap = await _context.RFIDMap
                .FirstOrDefaultAsync(m => m.RFIDMapId == id);
            if (rfidMap == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .Where(s => s.StudentId == rfidMap.StudentId)
                .SingleOrDefaultAsync();

            return View(new RFIDMapModel { rfidMap = rfidMap, Student = student });
        }

        // POST: Admin/Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rfidMap = await _context.RFIDMap.FindAsync(id);
            _context.RFIDMap.Remove(rfidMap);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RFIDMapExists(int id)
        {
            return _context.RFIDMap.Any(e => e.RFIDMapId == id);
        }
    }
}
