﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PPZ.Data
{
    public class Group
    {
        public int GroupId { get; set; }
        [Required]
        [Display(Name = "Name*")]
        public string Name { get; set; }
        public string Location { get; set; }
        [Display(Name = "Additional informations")]
        [DataType(DataType.MultilineText)]
        public string Info { get; set; }        

        public virtual List<GroupTeacher> GroupTeachers { get; set; }
        public virtual List<GroupStudent> GroupStudents { get; set; }
        public virtual List<GroupTerm> GroupTerms { get; set; }
    }
}
